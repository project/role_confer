********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Role Confer Module
Author: Robert Castelo <www.codepositive.com/robert>
Drupal: 5.x
********************************************************************
DESCRIPTION:

Confer a role on a user based on certain criteria:

 * Email address has been confirmed by user logging in or using one-time link
 * Specific fields have been filled in




********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire module directory into your Drupal modules/
   directory.

2. Enable the module by navigating to:

     administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.


********************************************************************
TO DO
Document API
Profile module integration





