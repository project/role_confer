<?php


/**
 * Implementation of hook_role_confer_configuration().
 */
function nodeprofile_role_confer_configuration($settings) {

	if (!module_exists('nodeprofile')) return; 
	
	$node_profile_types = nodeprofile_get_types('names');
	
	$form['nodeprofile'] = array(
		'#type' => 'fieldset',
		'#title' => t('Node Profile'),
		'#description' => t('Select nodeprofile content type(s) which must have their required fields filled in.'),
	);
	
	$form['nodeprofile']['nodeprofile_content_type'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Nodeprofile Content Type'),
		'#options' => $node_profile_types,
		'#default_value' => $settings['nodeprofile_content_type'],
	);

	return $form;
}

/**
 * Implementation of hook_role_confer_forms_affected().
 */
function nodeprofile_role_confer_forms_affected($settings) {

	foreach ($settings['nodeprofile_content_type'] as $key => $value) {
		$forms_id[] = $key . '_node_form';
	}	

	return $forms_id;
}

/**
 * Implementation of hook_role_confer_validate().
 * 
 * @ To Do - check required fields are all filled in
 */
function nodeprofile_role_confer_validate($settings, $form_values) {

	global $user;
	$types = $settings['nodeprofile_content_type'];
	
	// check user has a node of this type
	foreach ($types as $type) {
		$node = node_load(array('uid' => $user->uid, 'type' => $type));
		
		if (empty($node) && $form_values['type'] != $type) $missing[] = t('missing');
	}

	return $missing;
}

  


